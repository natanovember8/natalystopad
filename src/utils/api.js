import artistJson from "../MusicArtists"

export const updateData = (data) => localStorage.setItem('artist', JSON.stringify(data));
export const getData = () => {
    let res = JSON.parse(localStorage.getItem('artist'));
    if (!res){
        res = artistJson.artist_list.map(artist => {
            return {...artist.artist, Comments:""}
        });
        updateData(res);
    }
    return res;
};
