export const validate = (name, prior) => {
    let isError = false;
    const errors = {
      title: '',
      priority: '',
    };
    if (name === '' || !/\S[a-zа-я0-9А-ЯA-Z]$/.test(name)) {
      errors.title = 'Please enter valid title';
      isError = true;
    }
    if (prior === '') {
      errors.priority = 'Please enter valid priority';
      isError = true;
    }
    return {
      isError,
      errors,
    };
  };
  