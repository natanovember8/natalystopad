import React from 'react';

const DetailsHeader = ({modalFunc}) => {
  return(
    <div className="details-header">
      <div>
        Edit Diolog
      </div>
      <div>
        <button onClick={() => modalFunc()}>
          &times;
        </button>
      </div>
    </div>
  )
};

export default DetailsHeader;
