import React from 'react';

const DetailsFooter = ({modalFunc, submitFunc}) => {
  return(
    <div className="details-footer">
      <div>
        <button onClick={() => modalFunc()}>Cancel</button>
      </div>
      <div>
        <button onClick={submitFunc}>Submit</button>
      </div>
    </div>
  )
};

export default DetailsFooter;
