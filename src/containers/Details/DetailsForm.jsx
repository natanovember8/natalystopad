import React from 'react';

const DetailsForm = ({formInputs, changeFunc, errors}) => {
  return(
    <div className="details">
      <input
        type="text"
        value={formInputs.title}
        name="Id"
        onChange={changeFunc}
        placeholder="Id"
      />
      {errors.title.length !== 0 && <span className="errors">{errors.title}</span>}

      <input
        type="text"
        value={formInputs.name}
        name="Name"
        onChange={changeFunc}
        placeholder="Name"
      />

      <input
        type="text"
        value={formInputs.country}
        name="Country"
        onChange={changeFunc}
        placeholder="Country"
      />
      
      <input
        type="text"
        value={formInputs.uploaded}
        name="Uploaded at"
        onChange={changeFunc}
        placeholder="Uploaded at"
      />

      <input
        type="text"
        value={formInputs.twitter}
        name="Twitter URL"
        onChange={changeFunc}
        placeholder="Twitter URL"
      />

      {errors.priority.length !== 0 && <span className="errors">{errors.priority}</span>}

      <input
        type="text"
        value={formInputs.comments}
        name="Comments"
        onChange={changeFunc}
        placeholder="Twitter URL"
      />
    </div>
  )
};

export default DetailsForm;