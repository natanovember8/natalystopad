import React, {Component} from 'react';
import '../App.css';
import { getData } from '../utils/api';
import Table from "./Table";
import Search from "./Search";
import ReactDOM from "react-dom";
import Pagination from "react-js-pagination";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      artists: getData(),
      searchName:'',
      activePage: 2,
      posts: [],
      loading: false,
      currentPage: 1,
      postsPerPage:2,
    };
  }

  handleSearchChanges = (e) => {

    this.setState({
      searchName: e.target.value,
    });
  };
  
 

  render() {
 
    return (
        <div className="App container p-3">
          <div className="px-1 mb-2">
            <Search handleSearchChanges={this.handleSearchChanges}/>
          </div>

          <div>
            <Table artists={this.state.artists.filter(artist =>
                {return artist.artist_name.toLocaleLowerCase().includes(this.state.searchName.toLocaleLowerCase())}
            )}/>
          </div>
         
      <Pagination
        postsPerPage={postsPerPage}
        totalPosts={data.length}
        paginate = {this.paginate}
      />
        </div>
    );
  }
}

export default App;
