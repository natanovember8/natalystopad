import React, {Component} from 'react';

class Table extends Component {
    render() {
        return (
            <table className="table table-dark" data-pagination="true">
                <thead>
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Name</th>
                    <th scope="col">Country</th>
                    <th scope="col">Rating</th>
                    <th scope="col">Uploaded at</th>
                </tr>
                </thead>
                <tbody>
                {this.props.artists.map(artist => {
                    return (
                        <tr key={artist.artist_id}>
                            <th scope="row">{artist.artist_id}</th>
                            <td>{artist.artist_name}</td>
                            <td>{artist.artist_country}</td>
                            <td>{artist.artist_rating}</td>
                            <td>{new Date(artist.updated_time).toLocaleDateString()} {new Date(artist.updated_time)
                                .toLocaleTimeString('ua' , {timeStyle : "short"} )}</td>
                        </tr>
                    )
                })}
                </tbody>
            </table>
        );
        
    }
    
}

export default Table;
