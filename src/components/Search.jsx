import React, {Component} from 'react';

class Search extends Component {
    render() {
        return (
            <div className="input-group mb-3">
                <div className="input-group-prepend">
                    <span className="btn btn-primary" id="basic-addon1">Search by name</span>
                </div>
                <input onChange={this.props.handleSearchChanges} type="text" className="form-control" aria-label="name"
                       aria-describedby="basic-addon1"/>
            </div>
        );
    }
}

export default Search;
